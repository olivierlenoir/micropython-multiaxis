"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2019-10-29 09:12:13
License: MIT, Copyright (c) 2020-2022 Olivier Lenoir
Project: Multi Axis, MicroPython stepper motor
Description: This version of Multiaxis use GPIO
"""

from machine import Pin
from utime import sleep_us, ticks_us, ticks_diff

DRV8825_MP = const(2)


@micropython.viper
def sub(_a: int, _b: int) -> int:
    """Subtract integer"""
    return _a - _b


@micropython.viper
def do_step(_s: int, _d: int, _m: int) -> int:
    """Do step"""
    sdm = _s * _d + _m // 2
    return sdm // _m - (sdm - _d) // _m


# @micropython.native
class Axis:
    """Axis"""

    def __init__(self, dir_pin, step_pin):
        self.d_pin = Pin(dir_pin, Pin.OUT)
        self.s_pin = Pin(step_pin, Pin.OUT)
        self.pos = 0
        # Start period (µs)
        self.start_period = 50000
        # Target period (µs)
        self.period = 10000
        # Acceleration / Deceleration Rate (µs per step)
        self.acceleration_rate = 5000

    def set_dir(self, steps):
        """Set direction pin"""
        _d = self.d_pin()
        if steps < 0 and _d:
            self.d_pin(0)
        elif steps > 0 and not _d:
            self.d_pin(1)

    def step(self):
        """One step with position increment"""
        self.a_step()
        self.pos += 1 if self.d_pin() else -1

    def a_step(self):
        """Pulse step pin"""
        _sp = self.s_pin
        _sp(1)
        _sp(0)

    def steps(self, steps):
        """Many 'steps' with position increment"""
        # caching
        slp_us = sleep_us
        tck_us = ticks_us
        tck_dff = ticks_diff
        stp = self.a_step
        _mx = max
        # caching Acceleration / Deceleration
        str_p = self.start_period
        trg_p = self.period
        acc_r = self.acceleration_rate
        _acc = 0
        _dec = abs(steps) * acc_r - str_p
        # set direction
        self.set_dir(steps)
        # Loop steps
        rng = range(abs(steps))
        for _ in rng:
            t_0 = tck_us()
            stp()
            _acc += acc_r
            slp_us(_mx(0, _mx(trg_p, str_p - _acc, _acc - _dec) - tck_dff(tck_us(), t_0)))
        self.pos += steps

    def origin(self):
        """Move to origin"""
        self.steps(-self.pos)


# @micropython.native
class Multiaxis:
    """Multiaxis"""

    def __init__(self, *axis):
        self.axis = axis
        # Start period (µs)
        self.start_period = 50000
        # Target period (µs)
        self.period = 10000
        # Acceleration / Deceleration Rate (µs per step)
        self.acceleration_rate = 5000
        # Absolute positioning / incremental positioning
        self._abs_coord = True

    def cur_pos(self):
        """Current position"""
        return (a.pos for a in self.axis)

    def dists(self, coords):
        """Distance per axis from current position"""
        return map(sub, coords, self.cur_pos())

    def set_dirs(self, dists):
        """Set axis directions pins"""
        for axis, dist in zip(self.axis, dists):
            axis.set_dir(dist)

    def g01(self, *coord):
        """Linear interpolation"""
        # caching
        slp_us = sleep_us
        tck_us = ticks_us
        tck_dff = ticks_diff
        d_stp = do_step
        _mx = max
        # Absolute / incremental positioning
        dists = list(self.dists(coord)) if self._abs_coord else coord
        # max abs of dists
        m_dist = _mx(map(abs, dists))
        # set direction
        self.set_dirs(dists)
        # caching Acceleration / Deceleration
        str_p = self.start_period
        trg_p = self.period
        acc_r = self.acceleration_rate
        _acc = 0
        _dec = abs(m_dist) * acc_r - str_p
        # keep axis moving
        main_axis, other_axis, other_dists = [], [], []
        for axis, dist in zip(self.axis, dists):
            if abs(dist) == m_dist:
                main_axis.append(axis.a_step)
            elif dist:
                other_axis.append(axis.a_step)
                other_dists.append(dist)
        # Loop steps
        steps = range(1, m_dist + 1)
        for _s in steps:
            t_0 = tck_us()
            for axis in main_axis:
                axis()
            for axis, do_s in zip(other_axis, [d_stp(_s, d, m_dist) for d in other_dists]):
                if do_s:
                    axis()
            _acc += acc_r
            slp_us(_mx(0, _mx(trg_p, str_p - _acc, _acc - _dec) - tck_dff(tck_us(), t_0)))
        # update position
        for axis, dist in zip(self.axis, dists):
            axis.pos += dist

    def g90(self):
        """Absolute positioning"""
        self._abs_coord = True

    def g91(self):
        """Incremental positioning"""
        self._abs_coord = False
