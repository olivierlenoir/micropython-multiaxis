"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2019-12-08 14:26:49
License: MIT, Copyright (c) 2020-2022 Olivier Lenoir
Project: Testing 'multiaxis.py'
"""

from utime import ticks_ms, ticks_diff
from multiaxis import Axis, Multiaxis


x = Axis(dir_pin=2, step_pin=4)
y = Axis(dir_pin=16, step_pin=17)
z = Axis(dir_pin=5, step_pin=18)

cnc = Multiaxis(x, y, z)

t_ms = ticks_ms

print('period:', cnc.period, 'µs')

print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(100, -200, 11)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(117, -170, 60)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(100, -200, 17)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(117, -170, 30)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(0, -170, 40)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(100, -170, 40)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))


cnc.period = 200
print('period:', cnc.period, 'µs')

cnc.g01(0, 0, 0)
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(100, -200, 11)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(117, -170, 60)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(100, -200, 17)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))

start = t_ms()
cnc.g01(117, -170, 30)
print(ticks_diff(t_ms(), start), 'ms')
print(tuple(cnc.cur_pos()))
