"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2019-10-29 09:12:13
License: MIT, Copyright (c) 2020-2022 Olivier Lenoir
Project: Multi Axis, MicroPython stepper motor
Description: This version of Multiaxis use RMT
"""

from machine import Pin
from esp32 import RMT

DRV8825_MP = const(2)


@micropython.viper
def sub(_a: int, _b: int) -> int:
    """Subtract integer"""
    return _a - _b


@micropython.viper
def do_step(_s: int, _d: int, _m: int) -> int:
    """Do step"""
    sdm = _s * _d + _m // 2
    return sdm // _m - (sdm - _d) // _m


# @micropython.native
class Axis:
    """Axis"""

    id_axis = 0

    def __init__(self, dir_pin, step_pin):
        self.d_pin = Pin(dir_pin, Pin.OUT)
        self.s_pin = RMT(Axis.id_axis, pin=Pin(step_pin), clock_div=80)
        self._wp = self.s_pin.write_pulses
        Axis.id_axis += 1
        self.pos = 0
        # Start period (µs)
        self.start_period = 30000
        # Target period (µs)
        self.period = 10000
        # Acceleration / Deceleration Rate (µs per step)
        self.acceleration_rate = 5000

    def set_dir(self, steps):
        """Set direction pin"""
        _d = self.d_pin()
        if steps < 0 and _d:
            self.d_pin(0)
        elif steps > 0 and not _d:
            self.d_pin(1)

    def step(self, period=4):
        """One step with position increment"""
        self.a_step()
        self.pos += 1 if self.d_pin() else -1

    def a_step(self, period=4):
        """Pulse step pin"""
        self._wp((DRV8825_MP, max(DRV8825_MP, min(32767, period - DRV8825_MP))))

    def steps(self, steps, period=4, p_size=100):
        """Many 'steps' with position increment"""
        self.set_dir(steps)
        _d, _m = divmod(abs(steps), p_size)
        pulse = (DRV8825_MP, max(DRV8825_MP, min(32767, period - DRV8825_MP)))
        _wp = self._wp
        loops = range(_d)
        for _ in loops:
            _wp(pulse * p_size)
        if _m:
            _wp(pulse * _m)
        self.pos += steps

    def origin(self, period=0):
        """Move to origin"""
        self.steps(-self.pos, period)


# @micropython.native
class Multiaxis:
    """Multiaxis"""

    def __init__(self, *axis):
        self.axis = axis
        # Start period (µs)
        self.start_period = 30000
        # Target period (µs)
        self.period = 10000
        # Acceleration / Deceleration Rate (µs per step)
        self.acceleration_rate = 5000
        # Absolute positioning / incremental positioning
        self._abs_coord = True

    def cur_pos(self):
        """Current position"""
        return (a.pos for a in self.axis)

    def dists(self, coords):
        """Distance per axis from current position"""
        return map(sub, coords, self.cur_pos())

    def set_dirs(self, dists):
        """Set axis directions pins"""
        for axis, dist in zip(self.axis, dists):
            axis.set_dir(dist)

    def g01(self, *coord):
        """Linear interpolation"""
        # caching
        _mx = max
        # Absolute / incremental positioning
        dists = list(self.dists(coord)) if self._abs_coord else coord
        # max abs of dists
        m_dist = _mx(map(abs, dists))
        # set direction
        self.set_dirs(dists)
        # caching Acceleration / Deceleration
        str_p = self.start_period
        trg_p = self.period
        acc_r = self.acceleration_rate
        _acc = 0
        _dec = abs(m_dist) * acc_r - str_p
        # keep axis moving
        main_axis, other_axis, other_dists = [], [], []
        for axis, dist in zip(self.axis, dists):
            if abs(dist) == m_dist:
                main_axis.append(axis)
            elif dist:
                other_axis.append(axis)
                other_dists.append(dist)
        # Loop steps
        steps = range(1, m_dist + 1)
        for _s in steps:
            _acc += acc_r
            prd = _mx(trg_p, str_p - _acc, _acc - _dec)
            for axis in main_axis:
                axis.a_step(prd)
            for axis, do_s in zip(other_axis, [do_step(_s, d, m_dist) for d in other_dists]):
                if do_s:
                    axis.a_step()
        # update position
        for axis, dist in zip(self.axis, dists):
            axis.pos += dist

    def g90(self):
        """Absolute positioning"""
        self._abs_coord = True

    def g91(self):
        """Incremental positioning"""
        self._abs_coord = False
