"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-01-24 20:52:59
License: MIT, Copyright (c) 2020-2022 Olivier Lenoir
Project: Benchmark 'multiaxis.py'
"""

from utime import ticks_ms, ticks_diff
from multiaxis import Axis, Multiaxis


x = Axis(dir_pin=2, step_pin=4)
y = Axis(dir_pin=16, step_pin=17)
z = Axis(dir_pin=5, step_pin=18)
u = Axis(dir_pin=21, step_pin=19)

cnc = Multiaxis(x, y, z, u)

t_ms = ticks_ms

path = (
    (54321, 0, 0, 0),
    (54321, 54321, 54321, 0),
    (0, 0, 0, 0),
    (54321, 54321, 54321, 54321),
    (-54055, -1809, -6347, 98662),
    (35538, -44381, 505, -77378),
    (-156, -31516, 85753, -68355),
    (97549, -40291, 28639, -76738),
    (68695, 35280, 28639, -22840),
    (0, 0, 0, 0),
)

cnc.period = max(4, int(input('Period (µs): ')))
print('period: {}µs, freq: {:3.3f}kHz'.format(cnc.period, 1000 / cnc.period))

print('position: {}'.format(tuple(cnc.cur_pos())))

for point in path:
    print('=' * 60)
    dist = [t - p for t, p in zip(point, cnc.cur_pos())]
    print('distance: {}'.format(dist))
    start = t_ms()
    cnc.g01(*point)
    laps = ticks_diff(t_ms(), start)
    print('{} ms'.format(laps))
    print('freq (kHz): {}'.format([round(abs(d) / laps, 3) for d in dist]))
    print('target:   {}'.format(point))
    print('position: {}'.format(tuple(cnc.cur_pos())))
