TEXFILE = MicroPythonMultiaxis

all: doc clean

doc:
	pdflatex ${TEXFILE}.tex
	pdflatex ${TEXFILE}.tex

clean:
	rm ${TEXFILE}.aux
	rm ${TEXFILE}.log
	rm ${TEXFILE}.out
